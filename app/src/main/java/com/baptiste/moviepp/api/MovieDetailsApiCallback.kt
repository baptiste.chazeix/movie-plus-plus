package com.baptiste.moviepp.api

import androidx.lifecycle.MutableLiveData
import com.baptiste.moviepp.data.MovieDetails
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieDetailsApiCallback(val movie: MutableLiveData<MovieDetails>) : Callback<MovieDetails> {

    override fun onResponse(call: Call<MovieDetails>, response: Response<MovieDetails>) {
        if(response.isSuccessful){
            if(response.body() != null){
                movie.value = response.body()
            }
        }
    }

    override fun onFailure(call: Call<MovieDetails>, t: Throwable) {
        t.printStackTrace()
        movie.value = MovieDetails()
    }
}