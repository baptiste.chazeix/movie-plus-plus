package com.baptiste.moviepp.api

import android.graphics.BitmapFactory
import androidx.lifecycle.MutableLiveData
import com.baptiste.moviepp.data.Movie
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ImageMovieApiCallback(private val movieLiveData: MutableLiveData<Movie>, private val movie: Movie) : Callback<ResponseBody> {

    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
        if(response.isSuccessful){
            if(response.body() != null){
                movie.poster = BitmapFactory.decodeStream(response.body()!!.byteStream())
                movieLiveData.value = movie
            }
        }
    }

    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
        t.printStackTrace()
    }
}