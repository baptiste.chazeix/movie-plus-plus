package com.baptiste.moviepp.api

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ImageApiInterface {

    @GET("w500{image_url}")
    fun getPoster(@Path(value = "image_url", encoded = true) url: String): Call<ResponseBody>
}