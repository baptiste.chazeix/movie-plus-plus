package com.baptiste.moviepp.api

import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.baptiste.moviepp.data.Movie
import com.baptiste.moviepp.data.MovieDetails
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.InputStream

class ApiRepository {

    private val api = ApiController()
    private val imageApi = ImageApiController()

    fun getData() : LiveData<List<Movie>>{
        val list = ArrayList<Movie>()
        val data = MutableLiveData<List<Movie>>()
        val call = api.getNowPlayingMovies()
        var i=0

        call.enqueue(object: Callback<ApiResult> {

            override fun onResponse(call: Call<ApiResult>, response: Response<ApiResult>) {
                if(response.isSuccessful){
                    Log.d("movies", "response succesful !")
                    response.body()?.results?.forEach {
                        imageApi.getPoster(it.posterPath).enqueue(object: Callback<ResponseBody> {

                            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                Log.d("movies","image - onFailure")
                                t.printStackTrace()
                            }

                            override fun onResponse(call: Call<ResponseBody>, responseImage: Response<ResponseBody>) {
                                if (response.isSuccessful) {
                                    if (response.body() != null) {
                                        /***************************************/
                                        i += 1
                                        it.poster = BitmapFactory.decodeStream(responseImage.body()!!.byteStream())
                                        list.add(it)
                                        if(i == response.body()?.results?.size)
                                            data.value = list
                                        /***************************************/
                                    } else {
                                        Log.d("movies","image - null")
                                    }
                                } else {
                                    Log.d("movies","image - not successful")
                                }
                            }
                        })
                    }
                }
                else {
                    Log.d("movies", "response not succesful...")
                }
            }

            override fun onFailure(call: Call<ApiResult>, t: Throwable) {
                Log.d("movies", "error")
                t.printStackTrace()
            }
        })

        return data
    }

    fun getNowPlayingMovie() : MutableLiveData<List<Movie>>{
        val movies = MutableLiveData<List<Movie>>()
        val call = api.getNowPlayingMovies()
        call.enqueue(MovieApiCallback(movies))
        return movies
    }

    fun getMovieById(id: Long) : LiveData<MovieDetails>{
        val movie = MutableLiveData<MovieDetails>()
        val call = api.getMovieById(id)
        call.enqueue(MovieDetailsApiCallback(movie))
        return movie
    }

    fun setImageMovie(movie: Movie): LiveData<Movie> {
        val movieLiveData = MutableLiveData<Movie>()
        val call = imageApi.getPoster(movie.posterPath)
        call.enqueue(ImageMovieApiCallback(movieLiveData, movie))
        return movieLiveData
    }

    fun searchMovie(query: String, list : MutableLiveData<List<Movie>>) {
        val call = api.searchMovie(query)
        call.enqueue(MovieApiCallback(list))
    }

    fun reloadNowPlayingMovie(list: MutableLiveData<List<Movie>>){
        val call = api.getNowPlayingMovies()
        call.enqueue(MovieApiCallback(list))
    }

    fun setImage(poster: MutableLiveData<InputStream>, url: String) {
        imageApi.getPoster(url).enqueue(object: Callback<ResponseBody> {

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        poster.value = response.body()!!.byteStream()
                    }
                }
            }
        })
    }
}