package com.baptiste.moviepp.api

import androidx.lifecycle.MutableLiveData
import com.baptiste.moviepp.data.Movie
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieApiCallback(val movies: MutableLiveData<List<Movie>>) : Callback<ApiResult> {

    override fun onResponse(call: Call<ApiResult>, response: Response<ApiResult>) {
        if(response.isSuccessful){
            if(response.body() != null){
                movies.value = response.body()!!.results
            }
        }
    }

    override fun onFailure(call: Call<ApiResult>, t: Throwable) {
        movies.value = ArrayList()
        t.printStackTrace()
    }

}