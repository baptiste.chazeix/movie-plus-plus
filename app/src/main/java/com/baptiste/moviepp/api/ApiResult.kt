package com.baptiste.moviepp.api

import com.baptiste.moviepp.data.Movie

data class ApiResult(
    var results: List<Movie>,
    var page: Int,
    var total_results: Int,
    var total_pages: Int
)