package com.baptiste.moviepp.api

import com.google.gson.GsonBuilder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ImageApiController {

    companion object{
        const val POSTER_URL = "https://image.tmdb.org/t/p/"
    }

    private var api : ImageApiInterface

    init {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        val retrofit = Retrofit.Builder()
            .baseUrl(POSTER_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        api = retrofit.create(ImageApiInterface::class.java)
    }

    fun getPoster(url: String) : Call<ResponseBody> {
        return api.getPoster(url)
    }
}