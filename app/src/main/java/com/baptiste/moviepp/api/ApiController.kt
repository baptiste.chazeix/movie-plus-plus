package com.baptiste.moviepp.api

import com.baptiste.moviepp.data.MovieDetails
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiController {

    companion object{
        const val BASE_URL = "https://api.themoviedb.org/3/"
    }

    private var api : ApiInterface

    init {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        api = retrofit.create(ApiInterface::class.java)
    }

    fun getNowPlayingMovies() : Call<ApiResult>{
        return api.getNowPlayingMovies()
    }

    fun getMovieById(id: Long): Call<MovieDetails> {
        return api.getMovieById(id)
    }

    fun searchMovie(query: String): Call<ApiResult> {
        return api.searchMovie(query)
    }
}