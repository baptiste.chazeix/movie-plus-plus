package com.baptiste.moviepp.api

import com.baptiste.moviepp.data.MovieDetails
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {

    @GET("movie/now_playing?api_key=2ececd07e83a3e996743a05700344f7e")
    fun getNowPlayingMovies(): Call<ApiResult>

    @GET("movie/{id}?api_key=2ececd07e83a3e996743a05700344f7e")
    fun getMovieById(@Path(value = "id", encoded = true) id: Long): Call<MovieDetails>

    @GET("search/movie?api_key=2ececd07e83a3e996743a05700344f7e")
    fun searchMovie(@Query(value = "query", encoded = true) query: String): Call<ApiResult>
}