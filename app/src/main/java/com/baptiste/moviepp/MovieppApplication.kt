package com.baptiste.moviepp

import android.app.Application
import com.baptiste.moviepp.data.persistance.MovieDatabase

class MovieppApplication : Application(){
    override fun onCreate() {
        super.onCreate()
        MovieDatabase.initialize(this)
    }
}