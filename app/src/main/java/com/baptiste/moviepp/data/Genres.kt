package com.baptiste.moviepp.data

data class Genres(
    var id : Long = -1L,
    var name : String = ""
)