package com.baptiste.moviepp.data.persistance

import android.util.Log
import androidx.lifecycle.LiveData
import com.baptiste.moviepp.data.Movie
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 *  Un thread dédié à l'exécution asynchrone des méthodes
 *  de mise à jour des données dans la BdD.
 */
val IO_EXECUTOR: ExecutorService = Executors.newSingleThreadExecutor()

/**
 *  La source de vérité de l'information. Ici c'est juste un wrapper
 *  à la DAO qui permet d'exécuter en asynchrone les opérations qui
 *  le nécessitent.
 *  Dans la vraie vie on y fait aussi le choix de piocher l'information
 *  au bon endroit (BdD, fichier, source en remote, …)
 */
class MovieRepository(private val movieDao: MovieDao) {
    fun insert(movie: Movie) = IO_EXECUTOR.execute { Log.i("MovieRepo", "INSERT"); movieDao.insert(movie) }
    fun insertAll(movies: List<Movie>) = IO_EXECUTOR.execute { Log.i("MovieRepo", "INSERT_ALL"); movieDao.insertAll(movies)}
    fun delete(movie: Movie) = IO_EXECUTOR.execute { Log.i("MovieRepo", "DELETE"); movieDao.delete(movie) }

    fun exist(id: Long): LiveData<Boolean>{
        Log.i("MovieRepo","IS EXIST")
        return movieDao.exist(id)
    }

    fun getAll(): LiveData<List<Movie>> {
        Log.i("MovieRepo", "GET ALL MOVIES")
        return movieDao.getAll()
    }
}