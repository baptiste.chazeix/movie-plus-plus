package com.baptiste.moviepp.data

import android.graphics.Bitmap
import androidx.room.Ignore
import com.google.gson.annotations.SerializedName

data class MovieDetails(
    var id: Long = -1L,

    @SerializedName("original_title")
    var originalTitle: String = "",

    var title: String = "",

    @SerializedName("poster_path")
    var posterPath: String = "",

    @SerializedName("vote_average")
    var voteAverage: String = "",

    var overview: String = "",

    var budget: Long = -1L,

    var revenue: String = "",

    @SerializedName("backdrop_path")
    var backdropPath: String = "",

    var genres : List<Genres>? = null,

    @Ignore
    var poster: Bitmap? = null
) {
    fun isEmpty(): Boolean{
        return id == -1L
    }
}