package com.baptiste.moviepp.data

import android.graphics.Bitmap
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


const val NEW_MOVIE_ID = 0L

@Entity(tableName = "movies")
data class Movie(

    @PrimaryKey(autoGenerate = true)
    var id: Long = NEW_MOVIE_ID,

    @ColumnInfo(name = "title")
    @SerializedName("title")
    var title: String = "",

    @ColumnInfo(name = "poster_path")
    @SerializedName("poster_path")
    var posterPath: String = "",

    @Ignore
    var poster: Bitmap? = null

) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Movie

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}