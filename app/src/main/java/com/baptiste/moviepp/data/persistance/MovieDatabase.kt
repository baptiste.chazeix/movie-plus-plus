package com.baptiste.moviepp.data.persistance

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.baptiste.moviepp.MovieppApplication
import com.baptiste.moviepp.data.Movie

private const val MOVIE_DB_FILENAME = "movie_database.db"

@Database(entities = [Movie::class], version = 1, exportSchema = false)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun movieDAO(): MovieDao

    companion object {
        private var application: Application? = null
        @Volatile
        private var instance: MovieDatabase? = null

        fun getInstance(): MovieDatabase {
            if (application != null) {
                if (instance == null)
                    synchronized(this) {
                        if (instance == null)
                            instance = Room.databaseBuilder(
                                application!!.applicationContext,
                                MovieDatabase::class.java,
                                MOVIE_DB_FILENAME
                            ).build()
                    }
                return instance!!
            } else
                throw RuntimeException("the database must be first initialized")
        }


        @Synchronized
        fun initialize(app: MovieppApplication) {
            if (application == null)
                application = app
            else
                throw RuntimeException("the database must not be initialized twice")
        }
    }
}