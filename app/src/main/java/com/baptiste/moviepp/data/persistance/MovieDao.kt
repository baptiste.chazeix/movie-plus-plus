package com.baptiste.moviepp.data.persistance

import androidx.lifecycle.LiveData
import androidx.room.*
import com.baptiste.moviepp.data.Movie

@Dao
interface MovieDao {
    @Query("SELECT * FROM movies")
    fun getAll(): LiveData<List<Movie>>

    @Query("SELECT * FROM movies WHERE id = :id")
    fun findById(id: Long): LiveData<Movie>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: Movie)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(movies: List<Movie>)

    @Insert
    fun insertAll(vararg movie: Movie)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(movie: Movie)

    @Delete
    fun delete(movie: Movie)

    @Query("SELECT CASE WHEN EXISTS (SELECT * FROM movies WHERE id = :id) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END")
    fun exist(id: Long): LiveData<Boolean>
}