package com.baptiste.moviepp.ui.fragment

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.baptiste.moviepp.data.Movie
import com.baptiste.moviepp.ui.utils.MovieRecyclerViewAdapter
import com.baptiste.moviepp.ui.viewmodel.SharedViewModel

open class SimpleFragment : Fragment(), MovieRecyclerViewAdapter.Callbacks {

    @Suppress("LeakingThis")
    protected val moviesAdapter = MovieRecyclerViewAdapter(this)
    protected val viewModel : SharedViewModel by activityViewModels()
    private var listener: OnInteractionListener? = null

    override fun onMovieSelected(movieId: Long) {
        listener?.onMovieSelected(movieId)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    open fun updateList(movies: List<Movie>) {
        moviesAdapter.updateList(movies)
    }
}