package com.baptiste.moviepp.ui.activity

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.baptiste.moviepp.api.ApiRepository
import com.baptiste.moviepp.data.Movie
import com.baptiste.moviepp.data.MovieDetails
import com.example.moviepp.R
import kotlinx.android.synthetic.main.activity_movie.*
import java.io.InputStream

class MovieActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_MOVIE_ID = "com.baptiste.moviepp.extra_movieid"

        fun getIntent(context: Context, movieId: Long) =
            Intent(context, MovieActivity::class.java).apply {
                putExtra(EXTRA_MOVIE_ID, movieId)
            }
    }

    private var movieId = -1L
    private val apiRepository = ApiRepository()
    private lateinit var movie: LiveData<MovieDetails>
    private lateinit var poster: MutableLiveData<InputStream>
    private var loaded = false
    private var isFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieId = intent.getLongExtra(EXTRA_MOVIE_ID, -1L)

        MainActivity.movieRepository.exist(movieId).observe(this, Observer {
            isFavorite = it
        })

        movie = apiRepository.getMovieById(movieId)
        poster = MutableLiveData()

        poster.observe(this, Observer {
            updateImage(it)
        })

        movie.observe(this, Observer {
            progress_circular_movie_item.visibility = View.GONE
            if(it.isEmpty()){
                empty_movie_item.visibility = View.VISIBLE
                Toast.makeText(this, "Check your internet connection", Toast.LENGTH_LONG).show()
            } else {
                empty_movie_item.visibility = View.GONE
                content_movie_item.visibility = View.VISIBLE
                loaded = true
                updateView(it)
                getPoster(it.posterPath)
            }
        })

        setContentView(R.layout.activity_movie)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.favorite_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val id: Int = if (isFavorite) R.drawable.ic_favorite_fill_24dp else R.drawable.ic_favorite_border_24dp
        menu?.findItem(R.id.favorite_item)?.icon = ContextCompat.getDrawable(this, id)
        menu?.findItem(R.id.favorite_item)?.isVisible = true
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.favorite_item -> {
                if(loaded){
                    loaded = false
                    if(isFavorite){
                        MainActivity.movieRepository.delete(Movie(id = movieId))
                        item.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_24dp)
                        Toast.makeText(this, "Removed from favorites",Toast.LENGTH_SHORT).show()
                    } else {
                        MainActivity.movieRepository.insert(Movie(movieId, movie.value?.title ?: "Unknown", movie.value?.posterPath ?: "", null))
                        item.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_fill_24dp)
                        Toast.makeText(this, "Added to favorites",Toast.LENGTH_SHORT).show()
                    }
                    loaded = true
                }
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun getPoster(posterPath: String?){
        if(posterPath != null) apiRepository.setImage(poster, posterPath)
    }

    private fun updateImage(poster: InputStream?) {
        movie_image.setImageBitmap(BitmapFactory.decodeStream(poster))
    }

    private fun updateView(movie: MovieDetails) {
        if(movie.title.isBlank()){
            movie_title.text = resources.getString(R.string.unknown)
        } else {
            movie_title.text = movie.title
        }
        if(!movie.voteAverage.isBlank()){
            movie_average.text = movie.voteAverage
            movie_average_max.visibility = View.VISIBLE
        }
        if(movie.overview.isBlank()){
            movie_overview.text = resources.getString(R.string.unknown)
        } else {
            movie_overview.text = movie.overview
        }
    }


}