package com.baptiste.moviepp.ui.fragment

interface OnInteractionListener {
    fun onMovieSelected(movieId: Long)
}