package com.baptiste.moviepp.ui.activity

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.baptiste.moviepp.data.persistance.MovieDatabase
import com.baptiste.moviepp.data.persistance.MovieRepository
import com.baptiste.moviepp.ui.fragment.FavoriteFragment
import com.example.moviepp.R
import com.baptiste.moviepp.ui.fragment.HomeFragment
import com.baptiste.moviepp.ui.fragment.OnInteractionListener
import com.baptiste.moviepp.ui.fragment.SearchFragment
import com.baptiste.moviepp.ui.viewmodel.SharedViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.main_toolbar.*

class MainActivity : AppCompatActivity(), OnNavigationItemSelectedListener, OnInteractionListener {

    private lateinit var fragment: Fragment
    private lateinit var viewModel : SharedViewModel
    private val EXTRA_FRAGMENT_ID = "com.baptiste.moviepp.extra_fragmentid"

    companion object{
        val movieRepository = MovieRepository(MovieDatabase.getInstance().movieDAO())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(SharedViewModel::class.java)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        bottomNavigationView.selectedItemId = if (savedInstanceState?.getInt(EXTRA_FRAGMENT_ID) == null) R.id.home else savedInstanceState.getInt(EXTRA_FRAGMENT_ID)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(EXTRA_FRAGMENT_ID, bottomNavigationView.selectedItemId)
        super.onSaveInstanceState(outState)
    }

    private fun loadFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        fragment = when(item.itemId){
            R.id.home -> {
                reload.visibility = View.VISIBLE
                HomeFragment()
            }
            R.id.favorite -> {
                reload.visibility = View.GONE
                FavoriteFragment()
            }
            R.id.search -> {
                reload.visibility = View.GONE
                SearchFragment()
            }
            else -> {
                reload.visibility = View.VISIBLE
                HomeFragment()
            }
        }
        loadFragment()
        return true
    }

    override fun onMovieSelected(movieId: Long) {
        startActivity(MovieActivity.getIntent(this,movieId))
    }

    fun refresh(view: View) {
        if(bottomNavigationView.selectedItemId == R.id.home) {
            home_recycler_view.visibility = View.INVISIBLE
            empty_home.visibility = View.GONE
            progress_circular_home.visibility = View.VISIBLE
            viewModel.refreshHome()
        }
    }
}