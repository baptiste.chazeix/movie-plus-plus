package com.baptiste.moviepp.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.baptiste.moviepp.api.ApiRepository
import com.baptiste.moviepp.data.Movie
import com.baptiste.moviepp.data.persistance.MovieDatabase
import com.baptiste.moviepp.data.persistance.MovieRepository

class SharedViewModel : ViewModel() {

    private val databaseRepository = MovieRepository(MovieDatabase.getInstance().movieDAO())
    private val apiRepository = ApiRepository()


    private var homeIterator = 0
    var homeLoaded = false

    val favoriteMovies : LiveData<List<Movie>> = databaseRepository.getAll()
    var nowPlayingMovies : MutableLiveData<List<Movie>> = apiRepository.getNowPlayingMovie()
    var searchMovies = MutableLiveData<List<Movie>>()

    fun loadHomeImage(movie: Movie) : LiveData<Movie>? {
        return if(homeIterator != nowPlayingMovies.value?.size){
            homeIterator++
            @Suppress("SENSELESS_COMPARISON")
            return if(movie.posterPath != null)
                apiRepository.setImageMovie(movie)
            else null
        } else {
            homeLoaded = true
            null
        }
    }

    fun loadSearchImage(movie: Movie) : LiveData<Movie>? {
        @Suppress("SENSELESS_COMPARISON")
        return if(movie.posterPath != null)
            apiRepository.setImageMovie(movie)
        else null
    }

    fun search(query: String) {
        apiRepository.searchMovie(query, searchMovies)
    }

    fun refreshHome() {
        homeIterator = 0
        homeLoaded = false
        apiRepository.reloadNowPlayingMovie(nowPlayingMovies)
    }
}