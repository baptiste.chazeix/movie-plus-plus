package com.baptiste.moviepp.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.baptiste.moviepp.data.Movie
import com.example.moviepp.R
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : SimpleFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.nowPlayingMovies.observe(this, Observer {
            updateList(it)
            if(it.isEmpty()){
                empty_home.visibility = View.VISIBLE
                Toast.makeText(context, "Check your internet connection", Toast.LENGTH_LONG).show()
            } else {
                empty_home.visibility = View.GONE
                home_recycler_view.visibility = View.VISIBLE
                if(!viewModel.homeLoaded) loadImages()
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        view.home_recycler_view.layoutManager = StaggeredGridLayoutManager(2,LinearLayoutManager.VERTICAL)
        view.home_recycler_view.adapter = moviesAdapter
        return view
    }

    override fun updateList(movies: List<Movie>) {
        progress_circular_home.visibility = View.GONE
        moviesAdapter.updateList(movies)
    }

    private fun loadImages(){
        for (movie in viewModel.nowPlayingMovies.value!!){
            viewModel.loadHomeImage(movie)?.observe(this, Observer {
                moviesAdapter.update()
            })
        }
    }
}