package com.baptiste.moviepp.ui.utils

import android.content.res.Configuration
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.baptiste.moviepp.data.Movie
import com.example.moviepp.R
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieRecyclerViewAdapter(private val listener: Callbacks) : RecyclerView.Adapter<MovieRecyclerViewAdapter.MovieViewHolder>(){

    private var movies: List<Movie>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder =
        MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_movie,
                parent,
                false
            ), listener
        )


    override fun getItemCount() = movies?.size ?: 0

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(movies!![position])
    }

    fun updateList(movies: List<Movie>) {
        this.movies = movies
        notifyDataSetChanged()
    }

    fun update(){
        notifyDataSetChanged()
    }

    class MovieViewHolder(itemView: View, listener: Callbacks) : RecyclerView.ViewHolder(itemView) {

        var movie : Movie? = null
            private set

        init {
            itemView.setOnClickListener { movie?.let { listener.onMovieSelected(it.id) } }
        }

        fun bind(movie: Movie){
            this.movie = movie
            if(itemView.item_title != null)
                itemView.item_title.text = movie.title
            if(itemView.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT && movie.poster != null && itemView.image_view_movie != null) {
                itemView.image_view_movie.background = BitmapDrawable(itemView.resources, movie.poster)
            }
            else if(itemView.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE && movie.poster != null && itemView.image_view_movie != null){
                itemView.image_view_movie.setImageBitmap(movie.poster)
            }
        }
    }

    interface Callbacks {
        fun onMovieSelected(movieId: Long)
    }
}