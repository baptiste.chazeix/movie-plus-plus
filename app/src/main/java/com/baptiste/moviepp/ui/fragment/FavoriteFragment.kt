package com.baptiste.moviepp.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviepp.R
import kotlinx.android.synthetic.main.fragment_favorite.*
import kotlinx.android.synthetic.main.fragment_favorite.view.*

class FavoriteFragment : SimpleFragment() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.favoriteMovies.observe(this, Observer {
            updateList(it)
            if (it.isEmpty()){
                empty_favorite.visibility = View.VISIBLE
            } else {
                empty_favorite.visibility = View.GONE
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_favorite, container, false)
        view.favorite_recycler_view.layoutManager = LinearLayoutManager(context)
        view.favorite_recycler_view.adapter = moviesAdapter
        return view
    }
}