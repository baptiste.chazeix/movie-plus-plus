package com.baptiste.moviepp.ui.fragment

import android.app.Activity
import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.baptiste.moviepp.data.Movie
import com.example.moviepp.R
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_search.view.*

class SearchFragment : SimpleFragment() {

    lateinit var imm : InputMethodManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.searchMovies.observe(this, Observer {
            updateList(it)
            if(it.isEmpty()){
                empty_search.visibility = View.VISIBLE
            } else {
                empty_search.visibility = View.GONE
                search_recycler_view.visibility = View.VISIBLE
                loadImages()
            }
        })
        imm = context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        view.search_recycler_view.layoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
        view.search_recycler_view.adapter = moviesAdapter

        view.search_bar.onFocusChangeListener =
            View.OnFocusChangeListener { _, hasFocus ->
                if(hasFocus){
                    view.empty_search.visibility = View.GONE
                }
            }

        /**
         * Enlève le clavier une fois la recherche lancée
         */
        view.search_bar.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    val text: String = v?.text.toString()
                    imm.hideSoftInputFromWindow(view.windowToken,0)
                    v?.clearFocus()
                    if(text != ""){
                        empty_search.visibility = View.GONE
                        search_recycler_view.visibility = View.INVISIBLE
                        progress_circular_search.visibility = View.VISIBLE
                        viewModel.search(text)
                    }
                    return true
                }
                return false
            }
        })
        return view
    }

    override fun updateList(movies: List<Movie>) {
        progress_circular_search.visibility = View.GONE
        super.updateList(movies)
    }

    private fun loadImages(){
        for (movie in viewModel.searchMovies.value!!){
            viewModel.loadSearchImage(movie)?.observe(this, Observer {
                moviesAdapter.update()
            })
        }
    }
}