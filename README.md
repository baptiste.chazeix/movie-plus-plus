# Movie++

Projet d'application mobile sur Android afin de maîtriser toute les techniques de mapilution de données via API.
Elle permet de visualiser les derniers films à l'affiche, consulter une fiche détaillée d'un film mais également de rechercher un film qui n'est plus forcément au cinéma.
